import React from 'react';

const FormItem = ({ className = '', name, label, children }) => {
  return (
    <div className={`${className} form-item`}>
      {label &&<label htmlFor={name}>{label}</label>}
      {children}
    </div>
  );
};

export default FormItem;
