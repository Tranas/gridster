import React from 'react';

const NumberInput = ({ type="text", setValue, max, ...rest}) => {
   const handleChange = (evt) => {
    const numVal = evt.target.value * 1;
    if (isNaN(numVal)) {
        return;
    }
    if (numVal > max) {
        setValue(max);
        return;
    }
    if (numVal <= 0) {
        setValue(max);
        return;
    }
    setValue(numVal);
   };
   return <input onChange={handleChange} type={type} {...rest} />
};

export default NumberInput;
