const dijkstra = require('dijkstrajs');

export default (req, res) => {
  const body = JSON.parse(req.body);
  const { start, end, graph } = body;
  const { find_path } = dijkstra;
  var path = find_path(graph, start, end);
  res.status(200).json(path.reduce((prev, current) =>({ ...prev, [current]: current}), {}));
}
