import Head from 'next/head';
import React from 'react';
import classNames from 'classnames';

import FormItem from '../components/FormItem';
import NumberInput from '../components/NumberInput';

const INITIAL_SIZE = 10;
const MAX_GRID_SIZE = 20;

const calculateShortestPath = async (req) => {
  try {
    const res = await fetch('/api/shortestpath', {
      method: 'post',
      body: JSON.stringify(req),
    });
    if (!res.ok) {
      throw Error('Failed to fetch');
    }
    const data = await res.json();
    return data;
  } catch (err) {
    console.error(err.message, err);
  }
  return null;
};

const defaultBoxState = {
  start: null,
  end: null,
  cleared: null,
  path: null,
};

function generateMatrix (columns, rows) {
  let it = 0;
  const matrix = Array.from(Array(columns)).map(() => Array.from(Array(rows)).map(x => ({
    ...defaultBoxState,
    path: `v${it++}`
  })));
  return matrix;
};

function mapGridToGraph (boxes, columns, rows) {
 const graph = Array.from(Array(columns * rows ))
  .map((x, idx)=> idx).reduce((prev, current) => {
   return { ...prev, [`v${current}`]: {}}
 }, {});
 for (let i =  0; i< boxes.length; i++) {
  for (let j =  0; j< boxes[i].length; j++) {
    if (boxes[i][j + 1]) {
      graph[boxes[i][j].path][boxes[i][j + 1].path] = 1;
    }
    if (boxes[i][j-1]) {
      graph[boxes[i][j].path][boxes[i][j - 1].path] = 1;
    }
    if (boxes[i-1]) {
      graph[boxes[i][j].path][boxes[i-1][j].path] = 1;
    }
    if (boxes[i+1]) {
      graph[boxes[i][j].path][boxes[i+1][j].path] = 1;
    }
  }  
 }
 return graph;
};

function getRandomInt(min, max) {
  const num = Math.random() * (max - min) + min;
  return Math.floor(num);
}

function generateGrid(columns, rows) {
  const grid = generateMatrix(columns, rows);
  const xHalfMax = Math.floor(columns / 2);
  const yHalfMax = Math.floor(rows / 2);
  const [startY, startX] = [getRandomInt(0, yHalfMax), getRandomInt(0, xHalfMax)];
  grid[startX][startY].start = true;
  const [endY, endX] = [getRandomInt(yHalfMax, rows), getRandomInt(xHalfMax, columns)];
  grid[endX][endY].end = true;
  return { grid, start: grid[startX][startY].path, end: grid[endX][endY].path };
}

export default function Home() {
  const [columns, setColumns] = React.useState(INITIAL_SIZE);
  const [rows, setRows] = React.useState(INITIAL_SIZE);
  const [boxes, setBoxes] = React.useState([]);
  const [shortestPath, setPath] = React.useState({});
  React.useEffect(async () => { 
    const { grid, start, end } = generateGrid(columns, rows);
    setBoxes(grid);
    const graph = mapGridToGraph(grid, columns, rows);
    const response = await calculateShortestPath({ graph, start, end });
    setPath(response);
  },[])
  const handleClick = async (evt) => {
    evt.preventDefault();
    const { grid, start, end } = generateGrid(columns, rows);
    setBoxes(grid);
    const graph = mapGridToGraph(grid, columns, rows);
    const response = await calculateShortestPath({ graph, start, end });
    setPath(response);
  };
  const clearBox = (x,y) => {
    const copy = boxes.map((x) => (x.slice()));
    copy[x][y].cleared = copy[x][y].cleared ? false : true;
    setBoxes(copy);
  };
  return (
    <div className="container">
      <Head>
        <title>Gridster App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="main">
        <img className="logo" src="/Gridster-Logo.png" title="Gridster" />
        <div className="grid-props">
          <FormItem label="Rows" name="rows">
            <NumberInput max={MAX_GRID_SIZE} setValue={setRows}  value={rows} />
          </FormItem>
          <FormItem className="ml-2 mr-2">
            <span className="operand">x</span>
          </FormItem>
          <FormItem label="Columns" name="columns">
            <NumberInput max={MAX_GRID_SIZE} setValue={setColumns} value={columns} />
          </FormItem>
          <FormItem className="ml-2">
            <button onClick={handleClick} type="button">
              Generate
            </button>
          </FormItem>
        </div>
        <div className="grid">
         <table>
          <tbody>
           {boxes.map((x, xi) => (<tr key={`r${xi}`}>{x.map((y, yj)=>(<td 
             className={classNames({
               start: boxes[xi][yj].start,
               end: boxes[xi][yj].end,
               cleared: boxes[xi][yj].cleared,
               shortest: shortestPath[boxes[xi][yj].path] && !(boxes[xi][yj].start || boxes[xi][yj].end)
             })}
             onClick={() => {
               if (boxes[xi][yj].start || boxes[xi][yj].end || shortestPath[boxes[xi][yj].path]) {
                 return;
               }
             clearBox(xi,yj);
           }} key={boxes[xi][yj].path}></td>))}</tr>))}
          </tbody>
         </table>
        </div>
      </main>
    </div>
  );
}
